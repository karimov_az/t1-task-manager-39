package ru.t1.karimov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Update("INSERT INTO tm_user (" +
                "id, " +
                "created, " +
                "login, " +
                "password_hash, " +
                "role, " +
                "locked, " +
                "email, " +
                "first_name, " +
                "last_name, " +
                "middle_name" +
                ") " +
            "VALUES(" +
                "#{id}, " +
                "#{created}, " +
                "#{login}, " +
                "#{passwordHash}, " +
                "#{role}, " +
                "#{locked}, " +
                "#{email}, " +
                "#{firstName}, " +
                "#{lastName}, " +
                "#{middleName}" +
                ")"
    )
    void add(@NotNull User model);

    @Select("SELECT count(*) > 0 FROM tm_user WHERE id = #{id} LIMIT 1")
    boolean existsById(@NotNull String id);

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @NotNull
    List<User> findAll();

    @Select("SELECT * FROM tm_user " +
            "ORDER BY #{comparator}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @NotNull
    List<User> findAllSorted(@NotNull String comparator);

    @Select("SELECT * FROM tm_user " +
            "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @Nullable
    User findOneById(@NotNull String id);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @Nullable
    User findByLogin(@NotNull String login);

    @Select("SELECT count(*) FROM tm_user")
    int getSize();

    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
    })
    @Nullable
    User findByEmail(@NotNull String email);

    @Select("SELECT count(*) > 0 FROM tm_user WHERE login = #{login} LIMIT 1")
    boolean isLoginExist(@NotNull String login);

    @Select("SELECT count(*) > 0 FROM tm_user WHERE email = #{email} LIMIT 1")
    boolean isEmailExist(@NotNull String email);

    @Delete("DELETE FROM tm_user")
    void removeAll();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOne(@NotNull User model);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOneById(@NotNull String id);

    @Update("UPDATE tm_user SET" +
            " login = #{login}," +
            " password = #{passwordHash}, " +
            " role = #{role}," +
            " locked = #{locked}," +
            " email = #{email}," +
            " first_name = #{firstName}," +
            " last_name = #{lastName}," +
            " middle_name = #{middleName} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull User model);

}
