package ru.t1.karimov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.ISessionService;
import ru.t1.karimov.tm.exception.entity.EntityNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.IndexIncorrectException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.Session;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Session add(@Nullable Session model) throws  Exception {
        if (model == null) throw new EntityNotFoundException("session");
        @Nullable final String userId = model.getUserId();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.existById(userId, id);
        }
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAllByUserId(userId);
        }
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findOneById(userId, id);
        }
    }

    @Nullable
    @Override
    public Session findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @NotNull final List<Session> sessionList = findAll(userId);
        return sessionList.get(index);
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.getSizeByUserId(userId);
        }
    }

    @Override
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session removeOne(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException("session");
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeOne(model);
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session removeOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final Session session = sessionRepository.findOneById(userId, id);
            sessionRepository.removeOneById(userId, id);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @NotNull final List<Session> sessionList = findAll(userId);
        @Nullable final Session session = sessionList.get(index);
        removeOne(userId, session);
        return session;
    }

    @NotNull
    @Override
    public Session update(@Nullable final String userId, @Nullable final Session session) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new EntityNotFoundException();
        @NotNull SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (!existsById(userId, session.getId())) throw new EntityNotFoundException("session");
            sessionRepository.update(session);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
