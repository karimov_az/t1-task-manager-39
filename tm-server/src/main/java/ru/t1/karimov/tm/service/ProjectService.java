package ru.t1.karimov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IProjectService;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.field.*;
import ru.t1.karimov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private String getSortType(@NotNull final ProjectSort sort) {
        if (sort == ProjectSort.BY_CREATED) return "created";
        if (sort == ProjectSort.BY_STATUS) return "status";
        else return "name";
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project model) throws Exception {
        @Nullable final String userId = model.getUserId();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException("Error! Index оut of bounds...");
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.existsById(userId, id);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAll();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final ProjectSort sort
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (sort == null) return findAll(userId);
            return projectRepository.findAllByUserIdSorted(userId, getSortType(sort));
        }
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findOneById(userId, id);
        }
    }

    @Nullable
    @Override
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final List<Project> projects = projectRepository.findAllByUserId(userId);
            return projects.get(index);
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return (projectRepository.getSizeByUserId(userId));
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeOne(@Nullable final String userId, @Nullable final Project model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeOneById(userId, model.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeOneById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        @Nullable final Project result = findOneByIndex(userId, index);
        removeOne(userId, result);
    }

    @Override
    public void set(@NotNull final Collection<Project> projects) throws Exception {
        if (projects.isEmpty()) return;
        removeAll();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            for (Project project: projects) {
                repository.add(project);
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @NotNull
    @Override
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException("Error! Index оut of bounds...");
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

}
