package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws Exception;

    void removeProjectById(@Nullable String userId, @Nullable String projectId) throws Exception;

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws Exception;

}
