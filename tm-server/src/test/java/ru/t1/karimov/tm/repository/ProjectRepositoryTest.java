package ru.t1.karimov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static List<Project> projectList;

    @NotNull
    private static IProjectRepository projectRepository;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession connection = connectionService.getSqlSession();

    @NotNull
    private static final IUserRepository userRepository = connection.getMapper(IUserRepository.class);

    @BeforeClass
    public static void createUsers() throws Exception {
        @NotNull final User user1 = new User();
        user1.setLogin("test1");
        user1.setPasswordHash("test1");
        userRepository.add(user1);
        connection.commit();
        USER1_ID = user1.getId();

        @NotNull final User user2 = new User();
        user2.setLogin("test2");
        user2.setPasswordHash("test2");
        userRepository.add(user2);
        connection.commit();
        USER2_ID = user2.getId();
    }

    @Before
    public void initRepository() throws Exception {
        projectList = new ArrayList<>();
        projectRepository = connection.getMapper(IProjectRepository.class);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5) project.setUserId(USER1_ID);
            else project.setUserId(USER2_ID);
            projectList.add(project);
            projectRepository.add(project);
            connection.commit();
        }
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        userRepository.removeOneById(USER1_ID);
        connection.commit();
        userRepository.removeOneById(USER2_ID);
        connection.commit();
        connection.close();
    }

    @After
    public void initClear() throws Exception {
        for (@NotNull final Project project : projectList) {
            projectRepository.removeOne(project);
            connection.commit();
        }
        projectList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project();
        @NotNull final String userId = USER1_ID;
        @NotNull final String name = "Test Project Name";
        @NotNull final String description = "Test Project Description";
        @NotNull final String id = project.getId();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
        connection.commit();
        @Nullable final Project actualProject = projectRepository.findOneById(userId, id);
        assertNotNull(actualProject);
        assertEquals(userId, actualProject.getUserId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());

        projectRepository.removeOne(actualProject);
        connection.commit();
    }

    @Test
    public void testClearForUserPositive() throws Exception {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.removeAllByUserId(USER1_ID);
        connection.commit();
        assertEquals(emptyList, projectRepository.findAllByUserId(USER1_ID));
        assertNotEquals(emptyList, projectRepository.findAllByUserId(USER2_ID));
    }

    @Test
    public void testClearForUserNegative() throws Exception {
        projectRepository.removeAllByUserId("Other_id");
        connection.commit();
        final int numberAllProjects = projectRepository.getSizeByUserId(USER1_ID) + projectRepository.getSizeByUserId(USER2_ID);
        assertEquals(NUMBER_OF_ENTRIES, numberAllProjects);
    }

    @Test
    public void testFindAllByUser() throws Exception {
        @NotNull final List<Project> expectedProjects = projectList.stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Project> actualProjectList = projectRepository.findAllByUserId(USER1_ID);
        assertEquals(expectedProjects.size(), actualProjectList.size());
    }

    @Test
    public void testFindAllSortByUser() throws Exception {
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<Project> actualProjectList = projectRepository.findAllByUserIdSorted(USER1_ID, "name");
            @NotNull final List<Project> expectedProjectList = projectList.stream()
                    .filter(m -> USER1_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            for (int i = 0; i < actualProjectList.size(); i++) {
                @NotNull final Project actualProject = actualProjectList.get(i);
                @NotNull final Project expectedProject = expectedProjectList.get(i);
                assertEquals(actualProject.getId(), expectedProject.getId());
            }
        }
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<Project> actualProjectList = projectRepository.findAllByUserIdSorted(USER2_ID, "name");
            @NotNull final List<Project> expectedProjectList = projectList.stream()
                    .filter(m -> USER2_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            for (int i = 0; i < actualProjectList.size(); i++) {
                @NotNull final Project actualProject = actualProjectList.get(i);
                @NotNull final Project expectedProject = expectedProjectList.get(i);
                assertEquals(actualProject.getId(), expectedProject.getId());
            }
        }
    }

    @Test
    public void testFindOneByIdForUserPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = project.getId();
            @Nullable final Project actualProject = projectRepository.findOneById(userId, id);
            assertNotNull(actualProject);
            assertEquals(project.getId(), actualProject.getId());
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            assertNull(projectRepository.findOneById(USER1_ID, UUID.randomUUID().toString()));
        }
    }

    @Test
    public void testExistByIdForUserPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = project.getId();
            assertTrue(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testExistByIdForUserNegative() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = UUID.randomUUID().toString();
            assertFalse(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        final int expectedSize = NUMBER_OF_ENTRIES / userList.size();
        for (@NotNull final String userId : userList) {
            final int projectRepositorySize = projectRepository.getSizeByUserId(userId);
            @Nullable final List<Project> projectsUser = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(projectsUser.size(), projectRepositorySize);
            assertEquals(expectedSize, projectRepositorySize);
        }
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Project project : projectList) {
            projectRepository.removeOne(project);
            connection.commit();
            @Nullable final String userId =project.getUserId();
            assertNotNull(userId);
            assertNull(projectRepository.findOneById(userId, project.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        final int expectedSize = projectRepository.getSize();
        @NotNull final Project project = new Project();
        projectRepository.removeOne(project);
        connection.commit();
        final int actualSize = projectRepository.getSize();
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveByIdForUserPositive() throws Exception {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            assertNotNull(userId);
            projectRepository.removeOneById(userId, id);
            connection.commit();
            assertNull(projectRepository.findOneById(userId, project.getId()));
        }
    }

    @Test
    public void testRemoveByIdForUserNegative() throws Exception {
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        @NotNull final String otherId = UUID.randomUUID().toString();
        projectRepository.removeOneById(USER1_ID, otherId);
        connection.commit();
        assertNotNull(projectRepository.findOneById(USER1_ID, otherId));
        projectRepository.removeOneById(USER2_ID, otherId);
        connection.commit();
        assertNotNull(projectRepository.findOneById(USER2_ID, otherId));
        projectRepository.removeOneById(otherUserId,otherId);
        connection.commit();
        assertNotNull(projectRepository.findOneById(otherUserId, otherId));
    }

}
