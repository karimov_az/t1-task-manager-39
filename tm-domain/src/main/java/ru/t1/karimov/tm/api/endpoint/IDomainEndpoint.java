package ru.t1.karimov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.domain.*;
import ru.t1.karimov.tm.dto.response.domain.*;
import ru.t1.karimov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBackupLoadRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBackupSaveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBase64LoadRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBase64SaveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBinaryLoadRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataBinarySaveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonLoadFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonSaveFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonLoadJaxBResponse loadDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonLoadJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataJsonSaveJaxBResponse saveDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataJsonSaveJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlLoadFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlSaveFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlLoadJaxBResponse loadDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlLoadJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataXmlSaveJaxBResponse saveDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataXmlSaveJaxBRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataYamlLoadFasterXmlRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull DataYamlSaveFasterXmlRequest request
    ) throws AbstractException;

}
